import {inject, InjectionKey} from "vue";

export const autowire = <T>(key: InjectionKey<T>): T => {
    const value = inject(key);

    if (value === undefined) {
        throw new Error(`Impossible to inject ${key.toString()} with no value provided.`);
    }

    return value;
}

export const key = <T>(description: string): InjectionKey<T> => Symbol(description);