import {key} from "../Autowire.ts";
import {AccountRepository} from "./domain/AccountRepository.ts";

export const ACCOUNT_REPOSITORY = key<AccountRepository>('Account repository');