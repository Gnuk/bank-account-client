import {Plugin} from "vue";
import {ACCOUNT_REPOSITORY} from "./BankAccountKeys.ts";
import {AccountRest} from "./secondary/AccountRest.ts";

interface BankAccountOptions {
    hostUrl: string;
}

export const BankAccountProvider: Plugin<[BankAccountOptions]> = {
    install(app, {hostUrl}) {
        return app.provide(ACCOUNT_REPOSITORY, new AccountRest(hostUrl));
    }
}