import {Owner} from "./Owner.ts";
import {Amount} from "./Amount.ts";
import {AccountId} from "./AccountId.ts";
import {ResponseInfo} from "./ResponseInfo.ts";
import {Currency} from "./Currency.ts";

export interface AccountRepository {
    create(owner: Owner): Promise<ResponseInfo>;
    list(): Promise<ResponseInfo>;
    balance(id: AccountId, currency?: Currency): Promise<ResponseInfo>;
    deposit(id: AccountId, amount: Amount): Promise<ResponseInfo>;
    withdraw(id: AccountId, amount: Amount): Promise<ResponseInfo>;
}