import {describe, it, expect} from "vitest";
import {Amount} from "./Amount.ts";

describe('Amount', () => {
    it.each([
        {number: 1.5, formatted: '1.50'},
        {number: 22, formatted: '22'}
    ])('should format to $formatted for $number', ({number, formatted}) => expect(Amount.of(number).format).toBe(formatted));
});