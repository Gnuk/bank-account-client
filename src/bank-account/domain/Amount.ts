export class Amount {
    private constructor(private readonly amount: number) {

    }

    static of(amount: number): Amount {
        return new Amount(amount);
    }

    get format(): string {
        if(Number.isInteger(this.amount)) {
            return this.amount.toFixed(0);
        }
        return this.amount.toFixed(2);
    }
}