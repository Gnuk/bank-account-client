export const CURRENCIES = ['EUR', 'JPY'] as const;

export type Currency = typeof CURRENCIES[number];
