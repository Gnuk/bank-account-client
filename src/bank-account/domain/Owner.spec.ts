import {describe, expect, it} from "vitest";
import {Owner, OwnerEmpty} from "./Owner.ts";
import {Either} from "../../common/domain/Either.ts";

const evaluateError = <T>(either: Either<T, unknown>, expectation: (error: T) => void) => either.evaluate(expectation, () => {
    throw new Error('The either should apply an error');
});

const evaluateValue = <T>(either: Either<unknown, T>, expectation: (value: T) => void) => either.evaluate(() => {
    throw new Error('The either should apply value');
}, expectation);

describe('Owner', () => {
    it.each(['', ' ', '\t'])('should not be empty for "%s"', (empty) => evaluateError(Owner.of(empty), (error) => expect(error).toBe(OwnerEmpty)));
    it.each([
        {input: 'Jean', name: 'Jean'},
        {input: ' Louis\t', name: 'Louis'},
    ])('should normalise to $name for $input', ({input, name}) => evaluateValue(Owner.of(input), (owner) => expect(owner.name).toBe(name)));
});