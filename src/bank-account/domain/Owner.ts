import {Either} from "../../common/domain/Either.ts";

export const OwnerEmpty = Symbol('The owner is empty');

type OwnerError = typeof OwnerEmpty;

export class Owner {
    readonly name: string;
    private constructor(name: string) {
        this.name = name.trim();
        if(this.name.length === 0) {
            throw OwnerEmpty;
        }
    }

    static of(name: string): Either<OwnerError, Owner> {
        return Either.tryFrom(() => new Owner(name));
    }
}