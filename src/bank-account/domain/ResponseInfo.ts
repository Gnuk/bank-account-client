export interface ResponseInfo {
    status: number;
    body?: string;
}