export interface CreateAccountPayload {
    owner: string;
}

export interface AmountPayload {
    amount: string;
}
