import {AccountRepository} from "../domain/AccountRepository.ts";
import {Owner} from "../domain/Owner.ts";
import {Amount} from "../domain/Amount.ts";
import {
    AmountPayload, CreateAccountPayload,
} from "./AccountJson.ts";
import {AccountId} from "../domain/AccountId.ts";
import {ResponseInfo} from "../domain/ResponseInfo.ts";
import {Currency} from "../domain/Currency.ts";

const toBody = (response: Response): Promise<string | undefined> => response.json().then(json => JSON.stringify(json, undefined, 2)).catch(() => undefined);

const toResponseInfo = async (response: Response) => ({
    body: await toBody(response),
    status: response.status,
}) satisfies ResponseInfo;

const getResponse = (url: string): Promise<ResponseInfo> => fetch(url).then(toResponseInfo);

const postResponse = <Payload>(url: string, payload: Payload): Promise<ResponseInfo> => fetch(url, {
    method: 'POST',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
}).then(toResponseInfo);


function toCurrencyParams(currency?: Currency): string {
    if (currency === undefined) {
        return '';
    }

    return `?${new URLSearchParams({currency})}`;
}

export class AccountRest implements AccountRepository {
    constructor(private readonly hostUrl: string) {
    }

    async balance(id: AccountId, currency?: Currency): Promise<ResponseInfo> {
        return getResponse(`${this.hostUrl}/accounts/${id}${toCurrencyParams(currency)}`)
    }

    async create(owner: Owner): Promise<ResponseInfo> {
        return postResponse<CreateAccountPayload>(
            `${this.hostUrl}/accounts`,
            {owner: owner.name}
        );
    }

    deposit(id: AccountId, amount: Amount): Promise<ResponseInfo> {
        return postResponse<AmountPayload>(`${this.hostUrl}/accounts/${id}/deposit`, {
            amount: amount.format,
        });
    }

    list(): Promise<ResponseInfo> {
        return getResponse(`${this.hostUrl}/`);
    }

    withdraw(id: AccountId, amount: Amount): Promise<ResponseInfo> {
        return postResponse<AmountPayload>(`${this.hostUrl}/accounts/${id}/withdraw`, {
            amount: amount.format,
        });
    }
}