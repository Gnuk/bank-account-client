import { describe, it, expect, vi } from 'vitest';
import { Either, Ok, Err } from './Either';

describe('Either', () => {
    describe('Ok', () => {
        const good: Either<Error, string> = Either.ok('Good');
        it('Should get good value', () => {
            expect(good).toEqual(Ok.of('Good'));
        });

        it('Should map', () => {
            expect(good.map(right => `${right} Morning!`)).toEqual(Ok.of('Good Morning!'));
        });

        it('Should not map error', () => {
            expect(good.mapErr(err => new Error(err.message + '!'))).toEqual(Ok.of('Good'));
        });

        it('Should evaluate value', () => {
            const spyErr = vi.fn();
            const spyOk = vi.fn();

            good.evaluate(spyErr, spyOk);

            expect(spyErr).not.toHaveBeenCalled();
            expect(spyOk).toBeCalledWith('Good');
        });

        it('Should not evaluate when error', () => {
            const spyErr = vi.fn();

            good.whenErr(spyErr);

            expect(spyErr).not.toHaveBeenCalled();
        });

        it('Should evaluate when ok', () => {
            const spyOk = vi.fn();

            good.whenOK(spyOk);

            expect(spyOk).toBeCalledWith('Good');
        });

        it('Should convert a value', () => {
            expect(Either.tryFrom(() => 'Good')).toEqual(Ok.of('Good'));
        });

        it('should not throw', () => {
            expect(good.orThrow()).toBe('Good');
        });

        it('should get value with or else', () => {
            expect(good.orElse('other')).toBe('Good');
        });
    });

    describe('Error', () => {
        const ERROR = new Error('Something is wrong');
        const error: Err<Error, string> = Either.err(ERROR);

        it('Should be ko for bad value', () => {
            expect(error).toEqual(Err.of(ERROR));
        });

        it('Should not map', () => {
            expect(error.map(right => `${right} Morning!`)).toEqual(Err.of(ERROR));
        });

        it('Should map error', () => {
            expect(error.mapErr(err => new Error(err.message + '!'))).toEqual(Err.of(new Error('Something is wrong!')));
        });

        it('Should evaluate error', () => {
            const spyErr = vi.fn();
            const spyOk = vi.fn();

            error.evaluate(spyErr, spyOk);

            expect(spyErr).toBeCalledWith(ERROR);
            expect(spyOk).not.toHaveBeenCalled();
        });

        it('Should evaluate when error', () => {
            const spyErr = vi.fn();

            error.whenErr(spyErr);

            expect(spyErr).toBeCalledWith(ERROR);
        });

        it('Should not evaluate when ok', () => {
            const spyOk = vi.fn();

            error.whenOK(spyOk);

            expect(spyOk).not.toHaveBeenCalled();
        });

        it('Should convert a throwing error', () => {
            expect(
                Either.tryFrom(() => {
                    throw ERROR;
                })
            ).toEqual(Err.of(ERROR));
        });

        it('should throw', () => {
            expect(() => error.orThrow()).toThrow('Something is wrong');
        });

        it('should get other with or else', () => {
            expect(error.orElse('other')).toBe('other');
        });
    });
});