import { createApp } from 'vue'
import BankAccount from './bank-account/primary/BankAccount.vue'
import {BankAccountProvider} from "./bank-account/BankAccountProvider.ts";

createApp(BankAccount).use(BankAccountProvider, {hostUrl: '/api'}).mount('#app');
